# MoFEM is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# MoFEM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
# License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with MoFEM. If not, see <http://www.gnu.org/licenses/>

if(NOT METAIO_DIR)
  set(METAIO_DIR $ENV{METAIO_DIR})
endif(NOT METAIO_DIR)

if(METAIO_DIR)
  find_library(ITKMetaIO NAMES ITKMetaIO PATHS ${METAIO_DIR}/lib)
  find_library(itkzlib NAMES itkzlib PATHS ${METAIO_DIR}/lib/iktMetaIO)
  if(ITKMetaIO)
    include_directories(${METAIO_DIR}/include)
    include_directories(${METAIO_DIR}/include/iktMetaIO)
    include_directories(${METAIO_DIR}/include/iktMetaIO/Utilities)
    add_definitions(-DWITH_METAIO)
  endif(ITKMetaIO)
endif(METAIO_DIR)

if(WITH_METAIO)
  ExternalProject_Add(
    MetaIO
    PREFIX ${PROJECT_BINARY_DIR}/external/
    GIT_REPOSITORY https://github.com/likask/MetaIO.git
    GIT_TAG	ForMoFEMv0.2
    CMAKE_ARGS
      -DBUILD_SHARED_LIBS:BOOL=OFF
      -DCMAKE_INSTALL_PREFIX:PATH=${PROJECT_BINARY_DIR}/external/
    UPDATE_COMMAND ""
  )
endif(WITH_METAIO)

if(WITH_METAIO)
  if(NOT ITKMetaIO)
    include_directories(${PROJECT_BINARY_DIR}/external/include)
    include_directories(${PROJECT_BINARY_DIR}/external/include/iktMetaIO)
    include_directories(${PROJECT_BINARY_DIR}/external/include/iktMetaIO/Utilities)
    add_library(ITKMetaIO STATIC IMPORTED)
    set_property(TARGET ITKMetaIO PROPERTY IMPORTED_LOCATION ${PROJECT_BINARY_DIR}/external/lib/libITKMetaIO.a)
    add_dependencies(ITKMetaIO MetaIO)
    add_library(itkzlib STATIC IMPORTED)
    set_property(TARGET itkzlib PROPERTY IMPORTED_LOCATION ${PROJECT_BINARY_DIR}/external/lib/iktMetaIO/libitkzlib.a)
    add_dependencies(itkzlib MetaIO)
    add_definitions(-DWITH_METAIO)
  endif(NOT ITKMetaIO)
endif(WITH_METAIO)
