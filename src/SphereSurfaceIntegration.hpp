/** \file SphereSurfaceIntegration.hpp
* \ingroup bone_remodelling

* \brief Quadrature points on sphere surface

*/

/*
* This file is part of MoFEM.
* MoFEM is free software: you can redistribute it and/or modify it under
* the terms of the GNU Lesser General Public License as published by the
* Free Software Foundation, either version 3 of the License, or (at your
* option) any later version.
*
* MoFEM is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
* License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with MoFEM. If not, see <http://www.gnu.org/licenses/>. */

#ifndef __SPHERE_SURFACE_INETGRATION_HPP__
#define __SPHERE_SURFACE_INETGRATION_HPP__

namespace BoneRemodeling {

  /**
   * \brief 28 integration points on sphere
   *
   * @param  gauss_pts referenced matrix consisting normals at integration points
   * @param  weights Weights at gauss points

   * @return          Error code
   */
  PetscErrorCode sphereSurfaceIntegration28(
    MatrixDouble &gauss_pts,VectorDouble &weights
  );

  /**
   * \brief 21 integration points on sphere
   *
   * @param  gauss_pts referenced matrix consisting normals at integration points
   * @param  weights Weights at gauss points

   * @return          Error code
   */
  PetscErrorCode sphereSurfaceIntegration21(
    MatrixDouble &gauss_pts,VectorDouble &weights
  );

  /**
   * \brief 61 integration points on sphere
   *
   * @param  gauss_pts referenced matrix consisting normals at integration points
   * @param  weights Weights at gauss points

   * @return          Error code
   */
  PetscErrorCode sphereSurfaceIntegration61(
    MatrixDouble &gauss_pts,VectorDouble &weights
  );

}

#endif // __SPHERE_SURFACE_INETGRATION_HPP__
